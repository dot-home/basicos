//********************************************************************************
/*!
\author     Kraemer E
\date       27.01.2019

\file       OS_FIFO.h
\brief      Functions to handle ringbuffer creation and access

***********************************************************************************/

#ifndef _OS_FIFO_H_
#define _OS_FIFO_H_
    
/********************************* includes **********************************/
#include "OS_Config.h"

#if USE_OS_FIFO

#include "BaseTypes.h"
    
/***************************** defines / macros ******************************/
#define SINGLE_ENTRY    1
    
/****************************** type definitions *****************************/
/* Create enum for fifo status */
typedef enum 
{
  eFIFO_Error,
  eFIFO_OK
}teFifoResult;

// must be adapted (use unsigned short) if fifo is larger than 255 bytes
// e.g. typedef u16 T_FIFO_SIZE
// typedef u8 T_FIFO_SIZE;

typedef struct
{
    u8 ucSize;
    u8 ucFree;
    u8 ucGetIdx;
    u8 ucPutIdx;
    u8* pucBuffer;
}tsFIFO;

/***************************** global variables ******************************/


/************************ externally visible functions ***********************/

#ifdef __cplusplus
extern "C"
{
#endif

void OS_FIFO_Reset(tsFIFO* psFifo);
bool OS_FIFO_Create(u8 ucSize, tsFIFO* psFIFO, u8* pucBuffer);
teFifoResult OS_FIFO_Get(tsFIFO* psFifoStruct, u8* pucData);
teFifoResult OS_FIFO_Put(tsFIFO* psFifoStruct, u8 ucData);
teFifoResult OS_FIFO_RemovePutValues (tsFIFO* pFifo, u8 ucCount);
teFifoResult OS_FIFO_Peek(tsFIFO* psFifoStruct, u8* pucData);
teFifoResult OS_FIFO_GetBuffer(tsFIFO* psFifoStruct, u8* pucData, u8 ucCount, bool bUpdateFree);
teFifoResult OS_FIFO_PutBuffer(tsFIFO* psFifoStruct, u8* pucData, u8 ucCount, bool bUpdateFree);
teFifoResult OS_FIFO_PeekBuffer(tsFIFO* psFifoStruct, u8* pucData, u8 ucCount);
teFifoResult OS_FIFO_Overwrite(tsFIFO* psFifoStruct, u8* pucData, u8 ucCount);

#ifdef __cplusplus
}
#endif

#endif //USE_OS_FIFO

#endif //_OS_FIFO_H_
