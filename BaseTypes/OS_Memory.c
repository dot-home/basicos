//********************************************************************************
/*!
\author     Kraemer E
\date       09.07.2021

\file       OS_Memory.c
\brief      Functions to handle heap memory creation and access

***********************************************************************************/
#include "OS_Memory.h"

#if USE_OS_MEMORY

#include <string.h>
#include <stdlib.h>
#include "BaseTypes.h"

#include "OS_ErrorDebouncer.h"

#include "OS_Serial_UART.h"

/****************************************** Defines ******************************************************/
#define MEMORY_BLOCKS_CNT   32u

typedef struct
{
    uint32_t* pulAddress;       //Address in the heap
    uint16_t  uiBlockSize;      //Size of the memory block 
}tsMemoryBlock;

typedef struct
{
    tsMemoryBlock asMemoryBlocks[MEMORY_BLOCKS_CNT];
    int32_t slUsedHeap; //Total used size of the heap
}tsMemoryManagement;

/****************************************** Variables ****************************************************/
static tsMemoryManagement sMemoryManagement;

/****************************************** Function prototypes ******************************************/
static tsMemoryBlock* GetMemoryBlockByAddress(const uint32_t* pulAddress);
static tsMemoryBlock* GetNextEmptyBlock(void);

/****************************************** local functions *********************************************/

//********************************************************************************
/*!
\brief      Searches for the specific memory block with the same address.
\param      pulAddress - The address which shall be searched within the memory blocks
\return     psMemoryBlock - The reference to the memory block which contains the address
***********************************************************************************/
static tsMemoryBlock* GetMemoryBlockByAddress(const uint32_t* pulAddress)
{
    tsMemoryBlock* psMemoryBlock = NULL;

    for (int memoryBlockIdx = 0; memoryBlockIdx < _countof(sMemoryManagement.asMemoryBlocks); memoryBlockIdx++)
    {
        if (pulAddress == sMemoryManagement.asMemoryBlocks[memoryBlockIdx].pulAddress)
        {
            psMemoryBlock = &sMemoryManagement.asMemoryBlocks[memoryBlockIdx];
            break;
        }
    }

    return psMemoryBlock;
}

//********************************************************************************
/*!
\brief      Searches for an memory block which is empty.
\return     psMemoryBlock - The reference to the memory block which can be used
***********************************************************************************/
static tsMemoryBlock* GetNextEmptyBlock(void)
{
    tsMemoryBlock* psMemoryBlock = NULL;

    for (int memoryBlockIdx = 0; memoryBlockIdx < _countof(sMemoryManagement.asMemoryBlocks); memoryBlockIdx++)
    {
        if (sMemoryManagement.asMemoryBlocks[memoryBlockIdx].pulAddress == NULL)
        {
            psMemoryBlock = &sMemoryManagement.asMemoryBlocks[memoryBlockIdx];
            break;
        }
    }

    return psMemoryBlock;
}

/****************************************** External visible functions **********************************/

//********************************************************************************
/*!
\brief      Initializes the memory module
***********************************************************************************/
void OS_Memory_Init(void)
{
    if (sMemoryManagement.slUsedHeap)
    {
        OS_Memory_Restart();
    }
}


//********************************************************************************
/*!
\brief      Releases the reserved memory which was saved in the memory management.
***********************************************************************************/
void OS_Memory_Restart(void)
{
    int32_t ulFreedMemory = sMemoryManagement.slUsedHeap;

    //Go trough each memory block which is saved
    for (int memoryBlockIdx = 0; memoryBlockIdx < _countof(sMemoryManagement.asMemoryBlocks); memoryBlockIdx++)
    {        
        //Check for valid address
        if (sMemoryManagement.asMemoryBlocks[memoryBlockIdx].pulAddress != NULL
            && sMemoryManagement.asMemoryBlocks[memoryBlockIdx].uiBlockSize != 0)
        {
            ulFreedMemory -= sMemoryManagement.asMemoryBlocks[memoryBlockIdx].uiBlockSize;

            //Release memory block
            OS_Memory_Release(&sMemoryManagement.asMemoryBlocks[memoryBlockIdx].pulAddress, sMemoryManagement.asMemoryBlocks[memoryBlockIdx].uiBlockSize);
        }
    }

    //Freed memory should now be zero, otherwise something went wrong
    if (ulFreedMemory != 0)
    {
        /* Invalid memory assignment */
        OS_ErrorDebouncer_PutErrorInQueue(eOsMemory_InvalidFree);
    }
}

//********************************************************************************
/*!
\author     Kraemer E
\date       09.07.2021
\brief      Releases the reserved memory. Also checks if the released memory
            is allowed to be released.
\return     eResult - eMemory_Error when something was wrong.
\param      pvMemoryAddr - The address for the memory manager.
\param      ucMemorySize - The size of the reserved memory which shall be released.
***********************************************************************************/
teMemoryResult OS_Memory_Release(uint32_t** pulMemoryAddr, uint16_t uiMemorySize)
{
    teMemoryResult eResult = eMemory_Error;
    
    if(pulMemoryAddr != NULL && *pulMemoryAddr != NULL && uiMemorySize != 0)
    {
        tsMemoryBlock* psMemoryBlock = GetMemoryBlockByAddress(*pulMemoryAddr);

        if (psMemoryBlock != NULL)
        {
            /* Free heap memory */
            free(*pulMemoryAddr);

            /* Overwrite address. Heap memory is free but still can be accessed */
            *pulMemoryAddr = NULL;

            /* Reset the memory block */
            psMemoryBlock->pulAddress = NULL;
            psMemoryBlock->uiBlockSize = 0;

            /* Change variable values */
            sMemoryManagement.slUsedHeap -= uiMemorySize;

            /* Check if release is correct */
            if (sMemoryManagement.slUsedHeap < 0)
            {
                /* Invalid memory assignment */
                OS_ErrorDebouncer_PutErrorInQueue(eOsMemory_InvalidFree);
            }
            else
            {
                eResult = eMemory_OK;
            }
        }
        else
        {
            /* Invalid memory assignment */
            OS_ErrorDebouncer_PutErrorInQueue(eOsMemory_InvalidFree);
        }        
    }
    return eResult;
}


//********************************************************************************
/*!
\author     Kraemer E
\date       09.07.2021
\brief      Reserves the heap memory. Also checks if the reservation was successfully
            by checking the memory address and for overflows.
\return     eResult - eMemory_Error when something was wrong. Otherwise OK
\param      pvMemoryAddr - The address for the memory manager. Will be overwritten
                           from the memory manager. Create only with NULL-Pointer!!! 
\param      ucMemorySize - The size of the memory which shall be reserved.
***********************************************************************************/
teMemoryResult OS_Memory_Create(uint32_t** pulMemoryAddr, uint16_t uiMemorySize)
{
    teMemoryResult eResult = eMemory_Error;
    
    if(pulMemoryAddr != NULL && *pulMemoryAddr == NULL && uiMemorySize != 0)
    {
        tsMemoryBlock* psMemoryBlock = GetNextEmptyBlock();

        if (psMemoryBlock != NULL)
        {
            /* Reserve heap memory */
            *pulMemoryAddr = malloc(uiMemorySize);

            /* Save the reserved address and size */
            psMemoryBlock->pulAddress = *pulMemoryAddr;
            psMemoryBlock->uiBlockSize = uiMemorySize;

            /* Change variable values */
            sMemoryManagement.slUsedHeap += uiMemorySize;

            /* Check if reservation is correct */
            if (sMemoryManagement.slUsedHeap > USABLE_HEAP_SIZE || *pulMemoryAddr == NULL)
            {
                #if USE_OS_SERIAL_UART
                    OS_Serial_UART_Printf("Memory invalid create");
                #endif

                /* Invalid memory assignment */
                OS_ErrorDebouncer_PutErrorInQueue(eOsMemory_InvalidCreate);
            }
            else
            {
                eResult = eMemory_OK;
            }
        }
        else
        {
            /* Invalid memory assignment */
            OS_ErrorDebouncer_PutErrorInQueue(eOsMemory_InvalidCreate);
        }
    }
    return eResult;
}

//********************************************************************************
/*!
\author     Kraemer E
\date       09.07.2021
\brief      Copies from the heap memory into the given buffer
\return     eResult - eMemory_Error when something was wrong. Otherwise OK
\param      pvMemoryAddr - The address where the data is saved in the heap.
\param      pucData - Pointer to the buffer where the data shall be copied into.
\param      ucSize - The amount of data in bytes which shall be copied.
***********************************************************************************/
teMemoryResult OS_Memory_GetBuffer(uint32_t* pulMemoryAddr, uint8_t* pucData, uint16_t uiSize)
{
    teMemoryResult eResult = eMemory_Error;
    
    if(pulMemoryAddr && pucData && uiSize)
    {
        /* Copy given size */
        memcpy(pucData, pulMemoryAddr, uiSize);
        eResult = eMemory_OK;
    }
    return eResult;
}


//********************************************************************************
/*!
\author     Kraemer E
\date       09.07.2021
\brief      Gets a single byte from the heap memory.
\return     eResult - eMemory_Error when something was wrong. Otherwise OK
\param      pvMemoryAddr - The address where the data is saved in the heap.
\param      pucData - Pointer to the buffer where the data shall be copied into.
***********************************************************************************/
teMemoryResult OS_Memory_Get(uint32_t* pulMemoryAddr, uint8_t* pucData)
{
    return OS_Memory_GetBuffer(pulMemoryAddr, pucData, 1);
}


//********************************************************************************
/*!
\author     Kraemer E
\date       09.07.2021
\brief      Copies from the given buffer into the heap memory
\return     eResult - eMemory_Error when something was wrong. Otherwise OK
\param      pvMemoryAddr - The address where the data is shall be saved in the heap.
\param      pucData - Pointer to the buffer where the data shall be copied from.
\param      ucSize - The amount of data in bytes which shall be copied.
***********************************************************************************/
teMemoryResult OS_Memory_PutBuffer(uint32_t* pulMemoryAddr, const uint8_t* pucData, uint16_t uiSize)
{
    teMemoryResult eResult = eMemory_Error;
    
    if(pulMemoryAddr && pucData && uiSize)
    {
        /* Copy given size */
        memcpy(pulMemoryAddr, pucData, uiSize);
        eResult = eMemory_OK;
    }
    return eResult;
}


//********************************************************************************
/*!
\author     Kraemer E
\date       09.07.2021
\brief      Copies the given data into the heap memory
\return     eResult - eMemory_Error when something was wrong. Otherwise OK
\param      pvMemoryAddr - The address where the data is shall be saved in the heap.
\param      pucData - Pointer to the buffer where the data shall be copied from.
***********************************************************************************/
teMemoryResult OS_Memory_Put(uint32_t* pulMemoryAddr, const uint8_t* pucData)
{
    return OS_Memory_PutBuffer(pulMemoryAddr, pucData, 1);
}


//********************************************************************************
/*!
\author     Kraemer E
\date       09.07.2021
\brief      Copies from the given buffer into the heap memory with an offset.
\return     eResult - eMemory_Error when something was wrong. Otherwise OK
\param      pvMemoryAddr - The address where the data is shall be saved in the heap.
\param      pucData - Pointer to the buffer where the data shall be copied from.
\param      ucSize - The amount of data in bytes which shall be copied.
***********************************************************************************/
teMemoryResult OS_Memory_Overwrite(uint32_t* pulMemoryAddr, const uint8_t* pucData, uint16_t uiSize, uint8_t ucOffset)
{
    teMemoryResult eResult = eMemory_Error;
    
    if(pulMemoryAddr && pucData && uiSize)
    {
        /* Copy given size */
        memcpy((pulMemoryAddr + ucOffset), pucData, uiSize);
        eResult = eMemory_OK;
    }
    return eResult;
}

#endif //USE_OS_MEMORY