//********************************************************************************
/*!
\author     Kraemer E
\date       27.01.2019

\file       OS_Memory.h
\brief      Functions to handle dynamic memory creation and access

***********************************************************************************/

#ifndef _OS_MEMORY_H_
#define _OS_MEMORY_H_
    
/********************************* includes **********************************/
#include "OS_Config.h"

#if USE_OS_MEMORY

#include "BaseTypes.h"
    
/***************************** defines / macros ******************************/

/****************************** type definitions *****************************/
/* Create enum for fifo status */
typedef enum 
{
  eMemory_Error,
  eMemory_OK
}teMemoryResult;

/***************************** global variables ******************************/


/************************ externally visible functions ***********************/
#ifdef __cplusplus
extern "C"
{
#endif

void           OS_Memory_Init(void);
void           OS_Memory_Restart(void);
teMemoryResult OS_Memory_Release(uint32_t** pulMemoryAddr, uint16_t uiMemorySize);
teMemoryResult OS_Memory_Create(uint32_t** pulMemoryAddr, uint16_t uiMemorySize);
teMemoryResult OS_Memory_Get(uint32_t* pulMemoryAddr, uint8_t* pucData);
teMemoryResult OS_Memory_GetBuffer(uint32_t* pulMemoryAddr, uint8_t* pucData, uint16_t uiSize);
teMemoryResult OS_Memory_Put(uint32_t* pulMemoryAddr, const uint8_t* pucData);
teMemoryResult OS_Memory_PutBuffer(uint32_t* pulMemoryAddr, const uint8_t* pucData, uint16_t uiSize);
teMemoryResult OS_Memory_Overwrite(uint32_t* pulMemoryAddr, const uint8_t* pucData, uint16_t uiSize, uint8_t ucOffset);


#ifdef __cplusplus
}
#endif

#endif //USE_OS_MEMORY

#endif //_OS_MEMORY_H_
