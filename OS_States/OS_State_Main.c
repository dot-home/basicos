


/********************************* includes **********************************/
#include "OS_State_Main.h"
#if USE_OS_STATE_MAIN

#include "OS_StateManager.h"
#include "OS_Serial_UART.h"
#include "OS_Communication.h"
#include "OS_ErrorDebouncer.h"
#include "OS_ErrorHandler.h"
#include "OS_Config.h"
#include "OS_SoftwareTimer.h"
#include "OS_Flash.h"

#include "HAL_System.h"

/***************************** defines / macros ******************************/

/************************ local data type definitions ************************/
typedef struct
{
    u16 uiTimerInterval;
    u8  ucTimerIndex;
}tsSoftwareTimer;


/************************* local function prototypes *************************/
u8 GetSoftwareTimerIndex(u16 uiTimerIndex, u32 ulTimerInteval);


/************************* local data (const and var) ************************/
static tsSoftwareTimer sSoftwareTimer[MAX_EVENT_TIMER];
static u8 ucCreatedSoftwareTimer = 0;
/************************ export data (const and var) ************************/


/****************************** local functions ******************************/

//***************************************************************************
/*!
\author     KraemerE
\date       30.04.2021
\brief      Gets the timer entry of the HAL which is required for further
            operations.
\return     ucTimerEntry - The used timer entry of the HAL.
\param      eEventID - Event which shall be handled while in this state.
\param      uiTimerIndex - The timer index which is received from the HAL.
\param      ulTimerInterval - The timer interval of this timer.
******************************************************************************/
u8 GetSoftwareTimerIndex(u16 uiTimerIndex, u32 ulTimerInteval)
{
    u8 ucTimerEntry = 0xFF;
    
    /* When param 1 is greater than zero, than the parameter can be used as the timer index */
    if(uiTimerIndex != 0)
    {
        ucTimerEntry = uiTimerIndex;
    }
    else
    {
        /* Otherwise the timer index has to be searched with the given timer interval (=Param2) */
        u8 ucIdx = 0;
        for(ucIdx = 0; ucIdx < _countof(sSoftwareTimer); ucIdx++)
        {
            /* Check for same timer interval */
            if(sSoftwareTimer[ucIdx].uiTimerInterval == ulTimerInteval)
            {
                /* Copy the dependent timer index */
                ucTimerEntry = sSoftwareTimer[ucIdx].ucTimerIndex;
            }
        }
    }
    
    return ucTimerEntry;
}

/************************ externally visible functions ***********************/

//***************************************************************************
/*!
\author     KraemerE
\date       30.04.2021
\brief      Main state of the OS. In this state all OS-relevant events are handled
            before the event is used in the other states.
\return     u8
\param      eEventID - Event which shall be handled while in this state.
\param      uiParam1 - Event parameter from the received event
\param      ulParam2 - Second event parameter from the received event
******************************************************************************/
u8 OS_State_Main(teEventID eEventID, uiEventParam1 uiParam1, ulEventParam2 ulParam2)
{
    u8 ucReturn = EVT_PROCESSED;

    switch(eEventID)
    {
        case eEvtSoftwareTimer:
        {
            //Every 51ms
            if(ulParam2 == EVT_SW_TIMER_51MS)
            {
                /* Set error from error queue into error debouncer */
                OS_ErrorDebouncer_SetErrorInDebouncer();

                /* Check if an error can be send. Generates also an event */
                OS_ErrorHandler_SendErrorMessage();

                /* Handle saved frames in the retry-Message-Buffer */
                OS_Communication_Tick(SW_TIMER_51MS);
            }
            else if(ulParam2 == EVT_SW_TIMER_251MS)
            {
                /* Debounce errors */
                OS_ErrorDebouncer_DebounceErrors(SW_TIMER_251MS);
            }
            else if(ulParam2 == EVT_SW_TIMER_1001MS)
            {
                /* Call second retry tick from error handler */
                OS_ErrorHandler_OneSecRetryTimerTick();
            }
            break;
        }

        #if USE_OS_SERIAL_UART
        case eEvtSerialMsgReceived:
        {
            OS_Communication_HandleSerialCommEvent();
            break;
        }

        case eEvtSerialMsgSend:
        {
            /* Check if data is in TX-Buffer */
            if(uiParam1)
            {
                OS_Serial_UART_TransmitMessage();
            }
            break;
        }
        #endif

        case eEvtPower:
        case eEvtEnterBoot:
            break;

        case eEvtError:
        {
            tErrorPackage sErrorPackage;
            OS_ErrorDebouncer_GetErrorPackageFromParam(&ulParam2, &sErrorPackage);

            /* Use error handler */
            OS_ErrorHandler_HandleActualError(sErrorPackage.eFaultCode, sErrorPackage.bHandleError);
            break;
        }
        
        case eEvtErrorHandling:
        {            
            if(uiParam1 == eEvtParam_ErrorInvalidSlot || uiParam1 == eEvtParam_ErrorToManyErrors)
            {
                OS_Communication_SendDebugMessage("ErrDbcFault");
                //Send fault debug message
                HAL_System_SoftwareReset();
            }
        }

        case eEvtSoftwareTimerCreate:
        {
            //Param 1 is used for the Periodic or Suspended state and Param 2 is used for the desired time interval
            if(ucCreatedSoftwareTimer < _countof(sSoftwareTimer))
            {
                /* Set timer index first to invalid */
                sSoftwareTimer[ucCreatedSoftwareTimer].ucTimerIndex = INVALID_TIMER_INDEX;
                
                OS_SW_Timer_CreateTimer(&sSoftwareTimer[ucCreatedSoftwareTimer].ucTimerIndex, ulParam2, uiParam1);
                sSoftwareTimer[ucCreatedSoftwareTimer].uiTimerInterval = (u16)ulParam2;
                ++ucCreatedSoftwareTimer;
            }
            else
            {
                OS_ErrorDebouncer_PutErrorInQueue(eSoftwareTimer_TimerLimit);
            }
            break;
        }
        
        case eEvtSoftwareTimerAsyncCreate:
        {
            //Param 1 is the used time intervall and param 2 is the callback function of the timer
            if(ucCreatedSoftwareTimer < _countof(sSoftwareTimer))
            {
                /* Set timer index first to invalid */
                sSoftwareTimer[ucCreatedSoftwareTimer].ucTimerIndex = INVALID_TIMER_INDEX;
                
                OS_SW_Timer_CreateAsyncTimer(&sSoftwareTimer[ucCreatedSoftwareTimer].ucTimerIndex, uiParam1, eSwTimer_CreateSuspended, (void*)ulParam2);
                sSoftwareTimer[ucCreatedSoftwareTimer].uiTimerInterval = uiParam1;
                ++ucCreatedSoftwareTimer;
            }      
            else
            {
                OS_ErrorDebouncer_PutErrorInQueue(eSoftwareTimer_TimerLimit);
            }
            break;
        }
        
        case eEvtSoftwareTimerDelete:
        {
            //Param 1 is used for the timer index when known and Param 2 is used for the timer interval
            u8 ucTimerEntry = GetSoftwareTimerIndex(uiParam1, ulParam2);
            
            /* Delete timer. When no timer was found which could be deleted zero is returned */
            if(OS_SW_Timer_DeleteTimer(&ucTimerEntry) == 0)
            {
                OS_ErrorDebouncer_PutErrorInQueue(eSoftwareTimer_TimerLimit);
            }            
            break;
        }

        case eEvtSoftwareTimerStart:
        case eEvtSoftwareTimerSuspend:
        {
            //Param 1 is used for the timer index when known and Param 2 is used for the timer interval
            u8 ucTimerEntry = GetSoftwareTimerIndex(uiParam1, ulParam2);
            teSW_TimerStatus eTimerStateRequest = (u8)((eEventID == eEvtSoftwareTimerStart) ? eSwTimer_StatusRunning : eSwTimer_StatusSuspended);

            /* Check for correct retrieved timer entry */
            if(OS_SW_Timer_SetTimerState(ucTimerEntry, eTimerStateRequest) == 0)
            {
                OS_ErrorDebouncer_PutErrorInQueue(eSoftwareTimer_TimerLimit);
            }
            break;
        }
        
        case eEvtState_Request:
        {
            OS_StateManager_ChangeState((teStateList)uiParam1);
            break;
        }
        
        case eEvtExecuteSoftwareReset:
        {
            OS_Communication_SendDebugMessage("Prio1 error occured");
            HAL_System_SoftwareReset();
            break;
        }
        
        case eEvtFlashClearAll:
        {
            OS_Flash_EraseAll();
            HAL_System_SoftwareReset();
            break;
        }

        default:
            ucReturn = EVT_NOT_PROCESSED;
            break;
    }


    return ucReturn;
}

#endif //USE_OS_STATE_MAIN

