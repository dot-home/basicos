
//********************************************************************************
/*!
\author     Kraemer E.
\date       04.10.2020

\file       OS_STATE_RESET.h
\brief      Reset state for the basic operating system.

***********************************************************************************/
#ifndef _OS_STATE_RESET_H_
#define _OS_STATE_RESET_H_



/********************************* includes **********************************/
#include "OS_Config.h"
#if USE_OS_STATE_RESET

#include "BaseTypes.h"
#include "OS_EventManager.h"

/***************************** defines / macros ******************************/

/************************ externally visible functions ***********************/
#ifdef __cplusplus
extern "C"
{
#endif
    
u8 OS_State_Reset_Entry(teEventID eEventID, uiEventParam1 uiParam1, ulEventParam2 ulParam2);
u8 OS_State_Reset_Root(teEventID eEventID, uiEventParam1 uiParam1, ulEventParam2 ulParam2);
u8 OS_State_Reset_Exit(teEventID eEventID, uiEventParam1 uiParam1, ulEventParam2 ulParam2);

#ifdef __cplusplus
}
#endif

#endif //USE_OS_STATE_RESET

#endif // _OS_STATE_RESET_H_

/* [] END OF FILE */
