//********************************************************************************
/*!
\author     Kraemer E.
\date       04.10.2020

\file       RealTimeClock.h
\brief      Real time clock header file

***********************************************************************************/
#ifndef _REALTIMECLOCK_H_
#define _REALTIMECLOCK_H_



/********************************* includes **********************************/
#include "OS_Config.h"

#if USE_OS_REAL_TIME_CLOCK
#include "BaseTypes.h"

/***************************** defines / macros ******************************/

/************************ externally visible functions ***********************/
#ifdef __cplusplus
extern "C"
{
#endif
bool OS_RealTimeClock_Init(void);
void OS_RealTimeClock_SetTime(u32 ulEpochTick);
void OS_RealTimeClock_GetClock(u8* pucHour, u8* pucMin, u32* pulTicks);
bool OS_RealTimeClock_GetDaySaveTimeStatus(void);

#ifdef __cplusplus
}
#endif

#endif //USE_OS_REAL_TIME_CLOCK
#endif // _REALTIMECLOCK_H_

