//********************************************************************************
/*!
 \author     Kraemer E
 \date       06.02.2019

 \file       OS_SoftwareTimer.c
 \brief      Provides functions for cyclic execution of program parts from within
             the timer interrupt. Also posts periodic timer events.

 ***********************************************************************************/
#include "OS_Config.h"

#if USE_OS_SOFTWARETIMER
#include "OS_SoftwareTimer.h"
#include "HAL_Timer.h"
#include "OS_EventManager.h"
#include "OS_ErrorDebouncer.h"
#include "HAL_System.h"

#include "TargetConfig.h"
#ifdef ESPRESSIF_ESP8266
#include <Arduino.h>
#endif
/****************************************** Defines ******************************************************/


typedef struct
{
    pFunction pfnCallback;
    u16 uiRuntime;
    u16 uiTimeout;
    teSW_TimerCreate eCreateFlags;
}tsEventTimer;


/****************************************** Variables ****************************************************/
static tsEventTimer sEventTimers[MAX_EVENT_TIMER];



// milliseconds since system (timermgr) was started
static u32 ulSystemTickCount;

/****************************************** Function prototypes ******************************************/
static void SoftwareTimerCallback(void);
static void ProcessBasicTimers(void);
static teSW_TimerReturn CreateBasicTimer(u8* pucTimerIndex, u16 uiTimeout, teSW_TimerCreate eCreateFlag , pFunction pfnTimerCallback);

/****************************************** local functions *********************************************/
//********************************************************************************
/*!
 \author     Kraemer E
 \date       06.02.2019
 \brief      Checks whether a millisecond has passed. The posts a new timer event.
             Calls also the supervision function.
 \return     none
 ***********************************************************************************/
static void SoftwareTimerCallback(void)
{
    //Increment sytem counter
    ulSystemTickCount++;

    /* Process timers in the event-timer list */
    ProcessBasicTimers();
}

//********************************************************************************
/*!
 \author     Kraemer E
 \date       11.06.2021
 \brief      Creates a new timer entry in the list of timers. When an already used
             timer index is set the timeout or the callback function can be replaced.
 \param      pucTimerIndex - Pointer to a variable where the timer index is saved 
 \param      uiTimeout     - timeout of timer
 \param      eCreateFlag   - timer flags for creating a timer like CreateRunning or CreateSuspended
 \param      pfnTimerCallback - A callback function which shall be handled on match
 \return     eReturn   - eSwTimer_ReturnInvalid when not handled. 
                       - eSwTimer_ReturnIndexAlreadyUsed when already used index was set
                       - eSwTimer_ReturnProcessed when successfull
 ***********************************************************************************/
static teSW_TimerReturn CreateBasicTimer(u8* pucTimerIndex, u16 uiTimeout, teSW_TimerCreate eCreateFlag , pFunction pfnTimerCallback)
{
    u8 ucTimerIdx = INVALID_TIMER_INDEX;
    tsEventTimer* psTimer = &sEventTimers[0];
    teSW_TimerReturn eReturn = eSwTimer_ReturnInvalid;

    /* Check if timer index variable is available */
    if(pucTimerIndex)
    {
        /* Enter critical area, so disable interrupts and save them */
        HAL_System_EnterCriticalSection();
        
        /* Check if the timer index is already in use */
        if(*pucTimerIndex < _countof(sEventTimers) 
            && sEventTimers[*pucTimerIndex].eCreateFlags != eSwTimer_CreateEmpty)
        {
            /* Used timer found. Check for changes */
            if(sEventTimers[*pucTimerIndex].uiTimeout != uiTimeout)
            {
                /* Overwrite timeout */
                sEventTimers[*pucTimerIndex].uiTimeout = uiTimeout;
            }
            
            if(sEventTimers[*pucTimerIndex].pfnCallback != pfnTimerCallback)
            {
                /* Overwrite function callback */
                sEventTimers[*pucTimerIndex].pfnCallback = pfnTimerCallback;
            }
            
            eReturn = eSwTimer_ReturnIndexAlreadyUsed;
        }
        else
        {        
            /* Loop trough all timer-entries */
            for(ucTimerIdx = 0; ucTimerIdx < _countof(sEventTimers); ucTimerIdx++)
            {
                /* Check for an empty timer */
                if(psTimer->uiTimeout == 0)
                {
                    psTimer->uiTimeout = uiTimeout + TIMER_TICK_OFFSET;
                    psTimer->eCreateFlags = eCreateFlag;
                    psTimer->pfnCallback = pfnTimerCallback;

                    /* Don't enable the timer when timer is created as suspended (Runtime = 0)*/
                    if(eCreateFlag == eSwTimer_CreatePeriodic)
                    {
                        psTimer->uiRuntime = uiTimeout;
                    }

                    eReturn = eSwTimer_ReturnProcessed;
                    break;
                }

                /* Check next timer-entry */
                psTimer++;
            }
        }

        /* When the timer-index reached the limit, set it back to zero */
        if(ucTimerIdx >= _countof(sEventTimers))
        {
            ucTimerIdx = 0;
            OS_ErrorDebouncer_PutErrorInQueue(eOsTimerCreateFault);
        }

        /* Leave critical area and restore the previous saved interrupts */
        HAL_System_LeaveCriticalSection();

        /* Save timer index variable */
        *pucTimerIndex = ucTimerIdx;
    }
    
    return eReturn;
}

//********************************************************************************
/*!
 \author     Kraemer E
 \date       06.02.2019
 \brief      Loops through the list of timers and yields the appropriate actions.
             Function is usually called from interrupt context and thus does not
             need to enter a critical section during access to the timer structures.
 \return     none
 ***********************************************************************************/
static void ProcessBasicTimers(void)
{
    u8 ucTimerIdx = 0;
    tsEventTimer* psTimer = &sEventTimers[0];

    /* Loop trough all basic timers */
    for(ucTimerIdx = 0; ucTimerIdx < _countof(sEventTimers); ucTimerIdx++)
    {
        /* Check first if a timer is set */
        if(psTimer->eCreateFlags != eSwTimer_CreateEmpty)
        {
            /* Decrement timer until zero */
            if(psTimer->uiRuntime)
                --psTimer->uiRuntime;

            /* When runtime reached tick-offset handle the requested timer.
             A suspended timer will reach runtime = 0 but a periodic timer
             will be restarted again. */
            if(psTimer->uiRuntime == TIMER_TICK_OFFSET)
            {
                /* Check if there is a callback function */
                if(psTimer->pfnCallback)
                {
                    psTimer->pfnCallback();
                }
                /* Otherwise post an event */
                else
                {
                    /* Post software timer event. Param1 = Timer index and Param2 = Time-Interval */
                    OS_EVT_PostEvent(eEvtSoftwareTimer, ucTimerIdx, psTimer->uiTimeout);
                }

                /* If the timer set as a periodic timer, restart it.
                 A suspended timer should be restarted manually */
                if(psTimer->eCreateFlags == eSwTimer_CreatePeriodic)
                {
                    psTimer->uiRuntime = psTimer->uiTimeout;
                }
            }
        }

        /* Switch to next timer entry */
        psTimer++;
    }
}

/****************************************** External visible functiones **********************************/

//********************************************************************************
/*!
 \author     Kraemer E
 \date       06.02.2019
 \brief      Gets the number of milliseconds since the system was started.
 \return     ulTick - The current system tick count
 ***********************************************************************************/
u32 OS_SW_Timer_GetSystemTickCount(void)
{
    u32 ulTick;

    HAL_System_EnterCriticalSection();
    ulTick = ulSystemTickCount;
    HAL_System_LeaveCriticalSection();

    return ulTick;
}

//********************************************************************************
/*!
 \author     Kraemer E
 \date       06.02.2019
 \brief      Initializes the timer manager by setting up the timerbase and by setting
 the callback function for the timer interrupt.
 \return     void
 ***********************************************************************************/
bool OS_SW_Timer_Init(void)
{
    /* Initialize hardware timer and link the interrupt of the hardware timer to
     * the software routine.
     */
    bool bReturn = true;
    teSysTickReturn eReturn = HAL_Timer_Init(SoftwareTimerCallback);

    if(eReturn != eSysTick_Success)
    {
        bReturn = false;
    }

    return bReturn;
}


//********************************************************************************
/*!
 \author     Kraemer E
 \date       06.02.2019
 \brief      Creates a new software timer without a callback function
 \return     u8          - unique id of the timer
 \param      uiTimeout - timeout in millseconds
 \param      ucFlag    - TMF_CREATESUSPENDED, TMF_PERIODIC

 ***********************************************************************************/
teSW_TimerReturn OS_SW_Timer_CreateTimer(u8* pucTimerIndex, u16 uiTimeout, teSW_TimerCreate eCreateFlag)
{
    return CreateBasicTimer(pucTimerIndex, uiTimeout, eCreateFlag, NULL);
}

//********************************************************************************
/*!
 \author     Kraemer E
 \date       06.02.2019
 \brief      deletes an existing timer
 \return     u8        - 1 if timer exists and could be deleted
 \param      ucTimerId - 0 if no such timer exists
 ***********************************************************************************/
teSW_TimerReturn OS_SW_Timer_DeleteTimer(u8* pucTimerId)
{
    teSW_TimerReturn eReturn = eSwTimer_ReturnInvalid;

    if(pucTimerId)
    {
        // check for non zero timer
        if(*pucTimerId < _countof(sEventTimers))
        {
            /* Save adress of the timer which should be deleted */
            tsEventTimer* psTimer = &sEventTimers[*pucTimerId];

            /* Enter critical area, so disable interrupts and save them */
            HAL_System_EnterCriticalSection();

            // if this was really an existing timer
            if(psTimer->eCreateFlags != eSwTimer_CreateEmpty)
            {
                // mark timer structure as free by setting the relevant members to zero
                psTimer->uiRuntime = 0;
                psTimer->eCreateFlags = eSwTimer_CreateEmpty;
                psTimer->uiTimeout = 0;
                psTimer->pfnCallback = 0;
                *pucTimerId = INVALID_TIMER_INDEX;
                eReturn = eSwTimer_ReturnProcessed;
            }
            else
            {
                eReturn = eSwTimer_ReturnInvalidTimer;
            }

            /* Leave critical area and restore the previous saved interrupts */
            HAL_System_LeaveCriticalSection();
        }
    }
    
    if(eReturn != eSwTimer_ReturnProcessed)
    {
        OS_ErrorDebouncer_PutErrorInQueue(eOsTimerDeleteFault);
    }
    
    return eReturn;
}

//********************************************************************************
/*!
 \author     Kraemer E
 \date       06.02.2019
 \brief      Sets the state of a timer either to running or suspended
 \return     eReturn - Enumeration of the possible return values.
 \param      ucTimerId       - Timer index which shall be handled
 \param      eSetTimerState  - State to set the timer into. (Running or suspended)
 ***********************************************************************************/
teSW_TimerReturn OS_SW_Timer_SetTimerState(u8 ucTimerId, teSW_TimerStatus eSetTimerState)
{
    teSW_TimerReturn eReturn = eSwTimer_ReturnInvalid;

    // check for invalid flags
    if(eSetTimerState == eSwTimer_StatusRunning ||eSetTimerState == eSwTimer_StatusSuspended)
    {
        // if it is a valid timer and a valid state
        if(ucTimerId < _countof(sEventTimers))
        {
            /* Save adress of the timer */
            tsEventTimer* psTimer = &sEventTimers[ucTimerId];

            /* Enter critical area, so disable interrupts and save them */
            HAL_System_EnterCriticalSection();

            // if id is valid and it is not already running
            if(psTimer->uiTimeout != 0)
            {
                if(eSetTimerState == eSwTimer_StatusRunning)
                {
                    // restart it
                    psTimer->uiRuntime = psTimer->uiTimeout;
                }
                else
                {
                    // stop it
                    psTimer->uiRuntime = 0;
                }

                eReturn = eSwTimer_ReturnProcessed;
            }

            /* Leave critical area and restore the previous saved interrupts */
            HAL_System_LeaveCriticalSection();
        }
        else
        {
            eReturn = eSwTimer_ReturnInvalidTimer;
        }
    }

    return eReturn;
}

//********************************************************************************
/*!
 \author     Kraemer E
 \date       06.02.2019
 \brief      Checks whether a given timer is currently running or not
 \return     eTimerState - eSwTimer_StatusRunning, eSwTimer_StatusSuspened or 
                           eSwTimer_StatusInvalid (if timer does not exist)
 \param      ucTimerId   - id of timer to check (may be async)
 ***********************************************************************************/
teSW_TimerStatus OS_SW_Timer_GetTimerState(u8 ucTimerId)
{
    teSW_TimerStatus eTimerState = eSwTimer_StatusInvalid;

    // check for non zero timer
    if(ucTimerId < _countof(sEventTimers))
    {
        /* Get adress of the timer */
        tsEventTimer* psTimer = &sEventTimers[ucTimerId];

        /* Enter critical area, so save interrupt status and disable them */
        HAL_System_EnterCriticalSection();

        // if it is not already running
        if(psTimer->uiTimeout != 0)
        {
            if(psTimer->uiRuntime)
            {
                eTimerState = eSwTimer_StatusRunning;
            }
            else
            {
                eTimerState = eSwTimer_StatusSuspended;
            }
        }

        /* Leave critical area, so restore interrupt status and enable them */
        HAL_System_LeaveCriticalSection();
    }
    return eTimerState;
}

//********************************************************************************
/*!
 \author     Kraemer E
 \date       06.02.2019
 \brief      Creates a software timer, that will be called asynchronously (from IRQ context)
 \return     teSW_TimerReturn - Enum for the processed state
 \param      pucTimer - Pointer where the timer-index shall be saved.
 \param      uiTimeout - Timeout value for this timer 
 \param      eCreateFlag - The flag which shall be used for timer. (eSwTimer_CreateSuspended or eSwTimer_CreatePeriodic)
 \param      PFNTimerCallback  pfnCallback  - callback function
 ***********************************************************************************/
teSW_TimerReturn OS_SW_Timer_CreateAsyncTimer(u8* pucTimerIndex, u16 uiTimeout, teSW_TimerCreate eCreateFlag, pFunction pfnCallback)
{
    return CreateBasicTimer(pucTimerIndex, uiTimeout, eCreateFlag, pfnCallback);
}

#endif //USE_OS_SOFTWARETIMER