//********************************************************************************
/*!
\author     Schmidk (Ego/Ecs)
\date       25.03.2010

\file       TimerMgr.h
\brief      Provides functions for cyclic execution of program parts from within
            the timer interrupt. Also posts periodic timer events.

***********************************************************************************/
#ifndef _TIMERMGR_H_
#define _TIMERMGR_H_
    
/********************************* includes **********************************/
#include "OS_Config.h"
#if USE_OS_SOFTWARETIMER

#include "BaseTypes.h"

    
/***************************** defines / macros ******************************/
#define INVALID_TIMER_INDEX     0xFF
    
/* The Timer period is incremented by offset to handle 
 suspended timers easier */
#define TIMER_TICK_OFFSET   1  
/****************************** type definitions *****************************/
typedef enum
{
    eSwTimer_CreateEmpty     = 0x00,
    eSwTimer_CreateSuspended = 0x01,
    eSwTimer_CreatePeriodic  = 0x02
}teSW_TimerCreate;

typedef enum
{
    eSwTimer_StatusRunning   = 0x01,
    eSwTimer_StatusSuspended = 0x02,
    eSwTimer_StatusInvalid   = 0xFF
}teSW_TimerStatus;

typedef enum
{
    eSwTimer_ReturnProcessed         = 0,
    eSwTimer_ReturnInvalidTimer      = 1,
    eSwTimer_ReturnTimeoutToBig      = 2,
    eSwTimer_ReturnIndexAlreadyUsed  = 3,
    eSwTimer_ReturnInvalid       
}teSW_TimerReturn;

/***************************** global variables ******************************/


/************************ externally visible functions ***********************/

#ifdef __cplusplus
extern "C"
{
#endif

bool              OS_SW_Timer_Init (void);
u32               OS_SW_Timer_GetSystemTickCount (void);
teSW_TimerReturn  OS_SW_Timer_CreateTimer(u8* pucTimerIndex, u16 uiTimeout, teSW_TimerCreate eCreateFlag);
teSW_TimerReturn  OS_SW_Timer_CreateAsyncTimer(u8* pucTimerIndex, u16 uiTimeout, teSW_TimerCreate eCreateFlag, pFunction pfnCallback);
teSW_TimerReturn  OS_SW_Timer_SetTimerState(u8 ucTimerId, teSW_TimerStatus eTimerState);
teSW_TimerStatus  OS_SW_Timer_GetTimerState(u8 ucTimerId);
teSW_TimerReturn  OS_SW_Timer_DeleteTimer(u8* ucTimerId);

#ifdef __cplusplus
}
#endif

#endif //USE_OS_SOFTWARETIMER
#endif // TIMERMGR_H_
