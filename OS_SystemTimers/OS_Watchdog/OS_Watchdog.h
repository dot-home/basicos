/*
 * OS_Watchdog.h
 *
 *  Created on: 29.04.2021
 *      Author: kraemere
 */

#ifndef _OS_WATCHDOG_H_
#define _OS_WATCHDOG_H_
    
#include "OS_Config.h"   
#if USE_OS_WATCHDOG
    
/********************************* includes **********************************/
#include "BaseTypes.h"
#include "HAL_Watchdog.h"
    
/***************************** defines / macros ******************************/

/****************************** type definitions *****************************/

/***************************** global variables ******************************/

/************************ externally visible functions ***********************/

#ifdef __cplusplus
extern "C"
{
#endif

void OS_WDT_InitWatchdog(u16 uiResetInterval);
void OS_WDT_ClearWatchdogCounter(void);

void OS_WDT_SystemTimerInit(teSystemTimers eSystemTimer, u16 uiTimerValue);
void OS_WDT_SystemTimerDisableEnable(teSystemTimers eSystemTimer, bool bEnableDisable);
void OS_WDT_SystemTimerResetCounter(teSystemTimers eSystemTimer);
void OS_WDT_SystemTimerSetCallbackFunction(teSystemTimers eSystemTimer, void* pCallbackFunction);

void OS_WDT_SystemTimerInterruptInit(void);
void OS_WDT_SystemTimerInterruptEnableDisable(bool bEnableDisable);

#ifdef __cplusplus
}
#endif

#endif //#if USE_OS_WATCHDOG

#endif // _OS_WATCHDOG_H_

